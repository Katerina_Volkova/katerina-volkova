public class Employee extends Users {
    private double salary;
    private double experience;

    public Employee(String firstName, String secondName, String mobilePhone, int workPhone, String email, String password) {
        super(firstName, secondName, mobilePhone, workPhone, email, password);
        this.experience=experience;
        this.salary=salary;

    }
    public Employee(String password, String email, double experience, double salary) {
        super(password, email);
        this.experience=experience;
        this.salary=salary;
    }

    protected static double raisingSalary(double experience, double salary) {
        if (experience < 2 && experience>=0 ) {
            return salary*1.05;
        }
        if (experience >= 2 && experience <= 5) {
            return salary*1.1;
        }
        if (experience >5 ) {
            return salary*1.15;
        }else {return 0;
        }
    }

   public double getExperience() {
        return experience;
    }
public double getSalary (){
        return salary;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

}







