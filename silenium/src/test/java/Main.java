import org.junit.*;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.*;

public class Main {
   WebDriver driver = new ChromeDriver();

@BeforeClass
public static void beforeClass(){
    final String path = String.format("%s/bin/chromedriver.exe",
            System.getProperty("user.dir"));
    String s = System.setProperty("webdriver.chrome.driver", path);

}
@Before
public void preCondition(){
    driver.manage().window().maximize();
    driver.get("https://user-data.hillel.it/html/registration.html");
    //driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

}

    @Test
public void firstTest() throws InterruptedException {

        Users user = new Users();
        String firstName = user.getFirstName();
        String secondName = user.getSecondName();
        String mobilePhone = user.getMobilePhone();
        String workPhone = user.getWorkPhone();
        String password = user.getPassword();
        String email = user.getEmail();

        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.id("first_name")).sendKeys(user.getFirstName());
        driver.findElement(By.id("last_name")).sendKeys(user.getSecondName());
        driver.findElement(By.id("field_phone")).sendKeys(user.getMobilePhone());
        driver.findElement(By.id("field_work_phone")).sendKeys(user.getWorkPhone());
        driver.findElement(By.id("field_password")).sendKeys(user.getPassword());
        driver.findElement(By.id("field_email")).sendKeys(user.getEmail());
        driver.findElement(By.cssSelector(".radio")).click();
        driver.findElement(By.id("position")).click();
       Select position = new Select (driver.findElement(By.id("position")));
       position.selectByValue("manager");
       driver.findElement(By.cssSelector(".create_account")).click();

        WebDriverWait wait = new WebDriverWait(driver,25);
        wait.until(ExpectedConditions.alertIsPresent());

        Alert alert = driver.switchTo().alert();
        alert.accept();
//System.out.println(user.getPassword());
        driver.navigate().to("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.id("email")).sendKeys(user.getEmail());
        driver.findElement(By.id("password")).sendKeys(user.getPassword());
        driver.findElement(By.cssSelector(".login_button")).click();


        WebDriverWait wait1 = new WebDriverWait(driver,25);
        wait1.until(ExpectedConditions.elementToBeClickable(By.id("employees")));

        WebElement elementEmployees = driver.findElement(By.id("employees"));
        elementEmployees.click();

        driver.findElement(By.id("first_name")).sendKeys(user.getFirstName());
        driver.findElement(By.id("last_name")).sendKeys(user.getSecondName());
        driver.findElement(By.id("mobile_phone")).sendKeys(user.getMobilePhone());
        Select position1 = new Select(driver.findElement(By.id("position")));
        position1.selectByValue("manager");
        Select gender = new Select(driver.findElement(By.id("gender")));
        gender.selectByValue("female");
        driver.findElement(By.id("search")).click();


        /*WebDriverWait wait2 = new WebDriverWait(driver,10);
        wait2.until(ExpectedConditions.);*/

        Thread.sleep(5000);

        WebElement resultName = driver.findElement(By.xpath("//div/div[3]/table/tr/td[1]"));
        String actualResultName = resultName.getText();
//System.out.println(actualResultName);
        String expectedResultName = user.getFirstName();
        Assert.assertEquals(expectedResultName,actualResultName);
        WebElement resultLastname = driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[2]"));

        String actualResultLastname = resultLastname.getText();
//System.out.println(actualResultLastname);
        String expectedResultLastname = user.getSecondName();
        Assert.assertEquals(expectedResultLastname,actualResultLastname);

       WebElement resultEmail = driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[3]"));
       String actualResultEmail = resultEmail.getText();
       String expectedResultEmail = user.getEmail();
       Assert.assertEquals(expectedResultEmail,actualResultEmail);

        WebElement resultMobile = driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[4]"));
        String actualResultMobile = resultMobile.getText();
        String expectedResultMobile = user.getMobilePhone();
        Assert.assertEquals(expectedResultMobile,actualResultMobile);

        WebElement resultPhone = driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[5]"));
        String actualResultPhone = resultPhone.getText();
        String expectedResultPhone = user.getWorkPhone();
        Assert.assertEquals(expectedResultPhone,actualResultPhone);

        WebElement resultGender = driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[6]"));
        String actualResultGender = resultGender.getText();
        String expectedResultGender = "female";
        Assert.assertEquals(expectedResultGender,actualResultGender);

        WebElement resultPosition = driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[7]"));
        String actualResultPosition = resultPosition.getText();
        String expectedResultPosition = "manager";
        Assert.assertEquals(expectedResultPosition,actualResultPosition);

        driver.findElement(By.xpath("//div/div[3]/table/tr[1]/td[9]/input")).click();
        //Thread.sleep(5000);

        }
@After
    public void postCondition (){
    driver.quit();
}

}
