import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class Users {
    private final String firstName;
    private final String secondName;
    private final String mobilePhone;
    private final String workPhone;
    private final String email;
    private final String password;

    {

        firstName = "Kate" + new Random().nextInt(10);
        secondName = "Volkova" + new Random().nextInt(10);
        mobilePhone = "38093" + new Random().nextInt(10000000);
        workPhone = "044" + new Random().nextInt(1000000);
        email = generateEmail(4) + "@gmail.com";
        password = "Volkova" + new Random().nextInt(100);
    }

    public String getFirstName(){

    return firstName;
}


    public String getSecondName() {

        return secondName;
    }

    public String getMobilePhone() {

        return mobilePhone;
    }

    public String getWorkPhone() {

        return workPhone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;

    }
    private static final String SYMBOLS = "abcdefghijklmnopqrstuvwxyz";
    private char getSymbol() {
        int index = (int) (Math.random() * SYMBOLS.length());
        return SYMBOLS.charAt(index);
    }
    public String generateEmail(int length) {
        StringBuilder sb = new StringBuilder();
        while (length != 0) {
            sb.append((char) getSymbol());
            length--;
        }
        return sb.toString();
    }
    /*public static String generateRandomPassword(int len)
    {
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++)
        {
            int randomIndex = random.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex));
        }

        return sb.toString();
    }*/


}
