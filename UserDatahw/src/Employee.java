public class Employee extends Users {
    private double salary;
    private int experience;

    public Employee(String firstName, String secondName, String mobilePhone, int workPhone, String email, String password) {
        super(firstName, secondName, mobilePhone, workPhone, email, password);
        this.experience=experience;
        this.salary=salary;

    }
    public Employee(String password, String email, int experience, double salary) {
        super(password, email);
        this.experience=experience;
        this.salary=salary;
    }

    protected static double raisingSalary(int experience, double salary) {
        if (experience < 2) {
            return salary*1.05;
        }
        if (experience >= 2 && experience <= 5) {
            return salary*1.1;
        }
        else {
            return salary*1.15;
        }
    }

   public int getExperience() {
        return experience;
    }
public double getSalary (){
        return salary;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

}







